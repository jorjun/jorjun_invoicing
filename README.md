Jorjun Invoicing
===============

# Overview

jorjun_invoicing


Provenance
----------
I do not credit `django-simple-invoice` as a material inspiration, since no substantial features, except the addition of package distribution & a restrictive GNU license have been added to the commit log.
Having erroneously forked this project from the latest fork, I have since removed all of this effort, and including the GNU license which has been replaced with frictionless MIT which I deem a better alternative to the
absence of any license terms in the substantive original project: https://github.com/simonluijk/django-invoice.


Feature summary
---------------
Each invoice is based on an original order, and is the driver of invoice documents, so that for example a single order (say for monthly line rental) is capable of generating a new invoice every month.
Adhoc & other billing intervals are also supported.

Tenants are expected to setup their own invoicing clients & charge-items for their own order processing. User: A is unable to see or modify orders, invoices or charge items of User: B.


# Requirements:

* Python (2.7, 3.4)
* Django (1.7.3)

New database schema design & coding by [Jorjun](https://bitbucket.org/jorjun "@Jorjun") with sponsorship, specification & commercial verification given by Errol Samuels of Voiptology Ltd, England.


(c) 2015 Jorjun technical services Ltd, England
@jorjun April 21, 2015