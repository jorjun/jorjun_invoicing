import re
from os import path
from setuptools import setup, find_packages


def read(*parts):
    return open(path.join(path.dirname(__file__), *parts)).read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

setup(
    name='jorjun-invoicing',
    version=find_version('invoice', '__init__.py'),
    description='Multi-tenant billing for Django',
    author='@jorjun',
    license="MIT",
    long_description="Multi-tenant invoicing with Django & ReportLab PDF",
    keywords="tenant invoice django billing pdf",
    url='https://bitbucket.org/jorjun/jorjun_invoicing',
    packages=find_packages(),
    zip_safe=False,
    package_data={
        'menu_manager': [
            'locale/*/LC_MESSAGES/*',
            'templates/invoice/*',
            'static/*',
        ],
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
    install_requires=[
        'django>=1.7.3',
        'reportlab>=2.7',
        'PyPDF2>=1.20',
        'django-countries>=3.2',
        'django-extensions>=1.4.9',
        'Pillow>=2.7.0',
        'python-dateutil>=2.4.2',
        'reportlab==3.1.44',
        'requests==2.6.0',
        'six==1.9.0',
    ]
)
