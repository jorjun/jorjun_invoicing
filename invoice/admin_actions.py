# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

from invoice.models.invoice import *
from invoice.models.order import *
from invoice.models.prefs import *

import logging
log = logging.getLogger(__name__)


def send_invoice(self, request, queryset):
    pref = Preference.objects.get(tenant=request.user)
    for invoice in queryset.filter(status="1"):
        try:
            invoice.send_invoice(pref=pref)
        except KeyError as exc:
            self.message_user(request, exc, level=messages.ERROR)
            continue

    self.message_user(request, "Sent selected invoices", level=messages.INFO)


send_invoice.short_description = _(u"Send invoice to client")


def generate_invoice(self, request, queryset):
    log.info("Generate invoices")
    for order in queryset.filter():
        order.gen_invoice(request)
