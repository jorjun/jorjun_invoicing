from django.conf import settings
from django.http import HttpResponse
from hashlib import md5


def pdf_response(draw_funk, file_name, *args, **kwargs):
    response = HttpResponse(content_type="application/pdf")
    response["Content-Disposition"] = "attachment; filename=\"%s\"" % file_name
    draw_funk(response, *args, **kwargs)
    return response


def send_invoices():
    from invoice.models import Invoice

    for invoice in Invoice.objects.get_due():
        invoice.send_email()


def sign(*args):
    src = "".join(str(_) for _ in args + (settings.SECRET_KEY, ))
    return md5(src.encode("utf8")).hexdigest()