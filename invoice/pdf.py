from io import BytesIO
from email.mime.application import MIMEApplication

from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Table, Image as PlatImage
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from PIL import Image



class PDF(object):
    def __init__(self, path, invoice, pref):
        (self.path, self.invoice, self.pref) = path, invoice, pref
        self.logo = Image.open(self.pref.logo.file.name)
        if self.logo.format == "PNG":
            dpi_x, dpi_y = (72, 72)
        else:
            dpi_x, dpi_y= map(int, self.logo.info.get('dpi', (72, 72)))
        self.logo_width_cm = self.logo.size[0] / (dpi_x * 0.393701)
        self.logo_height_cm = self.logo.size[1] / (dpi_y * 0.393701)
        logo_y = -0.25 - self.logo_height_cm

        self.DIM = {
            "logo_y": logo_y * cm,
            "line_sep_y": (-0.50 + logo_y) * cm,
            "header": (13 * cm, (-2.20 + logo_y) * cm),
            "footer": (1 * cm, -27 * cm),
            "address": (1.5 * cm, (-2.20 + logo_y) * cm),
            "invoice_id_y": (-1.00 + logo_y) * cm,
            "title_y" : -3 * cm,
            "title_line_thick": 4,
            "invoice_x": 18 * cm,
            "table_y": (-7 + logo_y) * cm,
            "invoice": "Invoice",  # Must use unicode literal here
            "title_font": ('Helvetica-Bold', 16),
        }

    def __enter__(self):
        self.buffer = BytesIO()
        self.canvas = Canvas(self.buffer, pagesize=A4)
        self.canvas.translate(0, 29.7 * cm)
        self.canvas.setFont('Helvetica', 10)
        return self

    def __exit__(self, *args):
        inv = open(self.path, "wb")
        self.buffer.seek(0)
        inv.write(self.buffer.read())
        self.buffer.close()

    def get_mime_attachment(self):
        self.buffer.seek(0)
        attachment = MIMEApplication(self.buffer.read())
        attachment.add_header(
            "Content-Disposition", "attachment",
            filename=self.path
        )
        return attachment

    def draw_logo(self):
        DIM = self.DIM
        self.canvas.saveState()

        self.canvas.setStrokeColorRGB(0.9, 0.5, 0.2)
        self.canvas.setFillColorRGB(0.2, 0.2, 0.2)
        self.canvas.setFont(*DIM["title_font"])
        self.canvas.drawString(
            x=DIM["invoice_x"], y=DIM["title_y"], text=DIM["invoice"]
        )
        self.canvas.drawImage(
            image=self.pref.logo.file.name,
            x=1 * cm, y=DIM["logo_y"],
            mask='auto',
        )
        #self.canvas.setLineWidth(DIM["title_line_thick"])
        #self.canvas.line(0, DIM["line_sep_y"], 21.7 * cm, DIM["line_sep_y"])

        self.canvas.restoreState()

    def draw_header_footer(self, kind):
        """
        Header + footer
        """
        assert kind in ("header", "footer")
        self.canvas.saveState()
        textobject = self.canvas.beginText(*self.DIM[kind])
        for (idx, (hdr, text)) in enumerate(self.pref.header_footer_iter(kind=kind)):
            if  hdr:
                textobject.setFont('Helvetica-Bold', 9)
                textobject.textOut(hdr + " ")

            if  idx == 0:
                textobject.setFont('Helvetica-Bold', 9)
            else:
                textobject.setFont('Helvetica', 9)
            textobject.textOut(text)
            textobject.textLine()

        self.canvas.drawText(textobject)
        self.canvas.restoreState()


    def draw_pdf(self):
        DIM = self.DIM
        self.draw_logo()
        for kind in ("header", "footer"):
            self.draw_header_footer(kind=kind)

        # Client address
        client = self.invoice.order.recipient
        textobject = self.canvas.beginText(*DIM["address"])
        textobject.setFont('Helvetica-Bold', 9)
        textobject.textLine(client.company_name)
        textobject.setFont('Helvetica', 9)
        if client.address_line_1:
            textobject.textLine(client.address_line_1)
        if client.address_line_2:
            textobject.textLine(client.address_line_2)
        textobject.textLine(client.city)
        if client.state_province:
            textobject.textLine(client.state_province)
        textobject.textLine(client.postcode_zip)
        textobject.textLine(client.get_country_display())
        self.canvas.drawText(textobject)

        # Info
        textobject = self.canvas.beginText(1.5 * cm, DIM["invoice_id_y"])
        textobject.setFont('Helvetica-Bold', 9)
        textobject.textOut('Invoice ID')
        textobject.setFont('Helvetica', 9)
        textobject.textOut(' ' + str(self.invoice.invoice_id))
        textobject.textLine('')
        textobject.setFont('Helvetica-Bold', 9)
        textobject.textOut('Invoice Date: ')
        textobject.setFont('Helvetica', 9)
        textobject.textOut(
            self.invoice.invoice_date.strftime('%d %b %Y')
        )
        textobject.textLine('')
        self.canvas.drawText(textobject)

        # Items
        curr_format = self.invoice.order.currency.format
        data = [['Qty', 'Description', 'Amount', 'VAT', 'Tax 2', 'Total'], ]
        for item in self.invoice.items.all():
            (tax_1, tax_2) = item.tax("tax_1"), item.tax("tax_2")
            data.append([
                item.quantity,
                "{} {}".format(item.charge_item, item.charge_item.description),
                "{:.2f}".format(item.line_cost()),
                "{:.2f}".format(tax_1) if tax_1 else "",
                "{:.2f}".format(tax_2) if tax_2 else "",
                "{:.2f}".format(item.final_total()),
            ])
        data.append([''])

        data.append(['', '', '', '','TOTAL DUE:', curr_format(self.invoice.final_total())])
        table = Table(data, colWidths=[1 * cm, 9 * cm, 2.2 * cm, 2.2 * cm, 2.2 *cm])
        table.setStyle([
            ('FONT', (0, 0), (-1, -0), 'Helvetica-Bold'),
            ('FONTSIZE', (0, 0), (-1, -1), 10),
            ('TEXTCOLOR', (0, 0), (-1, -1), (0.2, 0.2, 0.2)),
            ('GRID', (0, 0), (-1, -2), 1, (0.7, 0.7, 0.7)),
            ('GRID', (-2, -1), (-1, -1), 1, (0.7, 0.7, 0.7)),
            ('ALIGN', (-4, 0), (-1, -1), 'RIGHT'),
            ('BACKGROUND', (0, 0), (-1, 0), (0.8, 0.8, 0.8)),
        ])
        tw, th, = table.wrapOn(self.canvas, 15 * cm, 20 * cm)
        table.drawOn(self.canvas, 0.7 * cm, DIM["table_y"] - th)

        self.canvas.showPage()
        self.canvas.save()
