from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django_countries.fields import CountryField

from .base import ModelBase, TenantBase


def upload_path(obj, name):
    _path = "img/prefs/{}_{}".format(obj.tenant.username, name)
    return _path


class Preference(TenantBase):
    """
    User preferences
            Bank Details: Street address, Town, County, POSTCODE
            Sort Code: 00-00-00 Account No: 00000000 (Quote invoice number)
            Please pay via bank transfer or cheque. All payments should be made in CURRENCY.',
            Make cheques payable to Company Name Ltd.
    """
    DETAILS = {
        "header": (
            "company_name",
            "company_address_1",
            "company_address_2",
            "company_city",
            'company_postcode',
            "company_country",
            "company_phone",
            "company_email",
            "company_site_url",
            "company_reg_no",
            "company_vat_reg_no",
        ),
        "footer": (
            "footer_1",
            "footer_2",
        ),
    }
    # These fields expect prefix e.g. Bank: xxxxxx  (where "Bank:" will be rendered in bold font)
    INVOICE_PREFIX_FIELDS = {
        "company_phone" : "company_phone_prefix",
        "company_email" : "company_email_prefix",
        "company_reg_no": "company_reg_prefix",
        "company_vat_reg_no": "company_vat_prefix",
    }

    # Company details
    logo = models.ImageField(upload_to=upload_path)
    company_name = models.CharField(_("name"), max_length=50)
    company_address_1 = models.CharField(_("address 1"), max_length=50)
    company_address_2 = models.CharField(_("address 2"), max_length=50, null=True, blank=True)
    company_city = models.CharField(("city"), max_length=50, null=True, blank=True)
    company_state = models.CharField(("state"), max_length=50, null=True, blank=True)
    company_postcode = models.CharField(_("postcode/zip"), max_length=15, null=True, blank=True)
    company_country = CountryField(_("country"), max_length=2)

    company_phone_prefix = models.CharField(_("phone prefix"), max_length=15, null=True, blank=True)
    company_phone = models.CharField(_("phone"), max_length=25, null=True, blank=True)

    company_email_prefix = models.CharField(_("email prefix"), max_length=15, null=True, blank=True)
    company_email = models.CharField(_("email"), max_length=40, null=True, blank=True)

    company_site_url = models.CharField(_("site_url"), max_length=50, null=True, blank=True)

    company_reg_prefix = models.CharField(_("reg. prefix"), max_length=15, null=True, blank=True)
    company_reg_no = models.CharField(_("reg. no."), max_length=25, null=True, blank=True)

    company_vat_prefix = models.CharField(_("tax prefix"), max_length=15, null=True, blank=True)
    company_vat_reg_no = models.CharField(_("tax ref."), max_length=25, null=True, blank=True)
    last_order_num = models.IntegerField(default=100100)
    last_invoice_num = models.IntegerField(default=100100)

    # Footer
    footer_1 = models.CharField(_("footer memo 1"), max_length=80, null=True, blank=True)
    footer_2 = models.CharField(_("footer memo 2"), max_length=80, null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.tenant)

    def header_footer_iter(self, kind):
        """
        Iterator for header / footer lines
        """
        assert kind in self.DETAILS
        for fld in self.DETAILS[kind]:
            (hdr, text) =  "", str(getattr(self, fld))
            if not text: continue
            if fld in self.INVOICE_PREFIX_FIELDS.values(): continue

            if fld in self.INVOICE_PREFIX_FIELDS:
                hdr = str(getattr(self, self.INVOICE_PREFIX_FIELDS[fld]))

            if fld == "company_country":
                text = self.get_company_country_display()
                yield hdr, text
                (hdr, text) = " ", " "  # Blank line after

            yield hdr, text


    def new_invoice_num(self):
        self.last_invoice_num += 1
        self.save()
        return self.last_invoice_num

    def new_order_num(self):
        self.last_order_num += 1
        self.save()
        return self.last_order_num

    def clean(self):
        from .invoice import Invoice
        from .order import Order

        # Order number already-used check
        if hasattr(self, "tenant"):
            last_num = int(self.last_order_num)
            _nums = sorted(
                [_.order_number for _ in \
                    Order.objects.filter(tenant=self.tenant, order_number__gt=last_num)[:9000] \
                    if int(_.order_number) > last_num
                ]
            )
            if _nums:
                raise ValidationError({
                    'last_order_num': 'An order found, ID: {}'.format(_nums[0])
                })

            # Invoice numbers already-used check
            last_num = int(self.last_invoice_num)
            _nums = sorted(
                [_.invoice_id for _ in \
                    Invoice.objects.filter(tenant=self.tenant, invoice_id__gt=last_num)[:9000] \
                    if int(_.invoice_id) > last_num
                ]
            )
            if _nums:
                raise ValidationError({
                    'last_invoice_num': 'An invoice exists, ID: {}'.format(_nums[0])
                })
