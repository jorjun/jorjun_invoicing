from datetime import date
from dateutil.relativedelta import relativedelta

from django.db import models
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.exceptions import ValidationError

from .base import ModelBase, TenantBase
from .charge_item import ChargeItem
from .invoice import Invoice, InvoiceItem
from .prefs import *
from .tax import Tax

import logging
log = logging.getLogger(__name__)


class OrderActiveManager(models.Manager):

    def get_queryset(self):
        return super(OrderActiveManager, self).get_queryset().filter(status="1", draft=False, close_date__isnull=True)


class Order(TenantBase):
    STATUS_CHOICE = (
        ("1", _("Active")),
        ("0", _("Inactive")),
        ("9", _("Archive")),
    )
    BILL_FREQ = {
        None: None,
        "WK1": relativedelta(weeks=1),
        "WK2": relativedelta(weeks=2),
        "WK4": relativedelta(weeks=4),
        "MN1": relativedelta(months=1),
        "MN2": relativedelta(months=2),
        "MN3": relativedelta(months=3),
        "MN6": relativedelta(months=6),
        "YR1": relativedelta(years=1),
        "YR2": relativedelta(years=2),
        "YR3": relativedelta(years=3),
    }
    BILL_FREQ_CHOICES = (
        (None, _("adhoc")),
        ("WK1", _("weekly")),
        ("WK2", _("fortnightly")),
        ("WK4", _("4 weekly")),
        ("MN1", _("monthly")),
        ("MN3", _("quarterly")),
        ("MN6", _("bi-annually")),
        ("YR1", _("yearly")),
    )

    status = models.CharField(choices=STATUS_CHOICE, max_length=1, default="1", db_index=True)

    recipient = models.ForeignKey("Client", verbose_name=_(u'recipient'))
    currency = models.ForeignKey("Currency", verbose_name=_(u'currency'))
    order_number = models.IntegerField(_(u'order number'), db_index=True, blank=True, default=0)
    last_invoice = models.ForeignKey("Invoice", blank=True, null=True, editable=False, related_name="last_invoice", on_delete=models.SET_NULL)
    draft = models.BooleanField(_("draft"), default=False)  # Invoiceable ?
    credit_invoice = models.OneToOneField("Invoice", blank=True, null=True,
                                           verbose_name=_(u'Credit invoice'),
                                           related_name="credit_node")
    start_date = models.DateField(_("Invoice start"), editable=True, default=date.today)
    close_date = models.DateField(null=True, blank=True)
    last_invoice_date = models.DateField(_('Last invoice date'), blank=True, null=True, editable=True)
    bill_frequency = models.CharField(_("How often"), max_length=3, choices=BILL_FREQ_CHOICES, null=True, blank=True)
    bill_times = models.IntegerField(_("How many"), blank=True, null=True)

    active_objects = OrderActiveManager()
    objects = models.Manager()

    class Meta:
        ordering = ("-order_number", )


    def __str__(self):
        return "Order#{} {}".format(self.order_number, self.recipient) or "***"

    @property
    def value(self):
        return sum([_.quantity * _.charge_item.unit_price for _ in self.order_lines.all()])

    def credit_note_related_link(self):
        if self.credit_note and not self.credit_invoice:
            change_url = urlresolvers.reverse(
                'admin:invoice_invoice_change', args=(self.credit_note.pk,)
            )
            return '<a href="%s">%s</a>' % (
                change_url, self.credit_note.invoice_id
            )
        else:
            return self.invoice_id

    credit_note_related_link.short_description = _(u"Credit note")
    credit_note_related_link.allow_tags = True

    def invoice_related_link(self):
        if self.credit_invoice:
            change_url = urlresolvers.reverse('admin:invoice_invoice_change',
                                              args=(self.credit_invoice.pk,))
            return '<a href="%s">%s</a>' % (change_url,
                                            self.credit_invoice.invoice_id)
        else:
            return self.invoice_id
    invoice_related_link.short_description = _(u"Invoice")
    invoice_related_link.allow_tags = True


    def save(self, *args, **kwargs):
        if not self.order_number:
            super(Order, self).save(*args, **kwargs)  # New self.pk
            _pref = Preference.objects.get(tenant=self.tenant)
            self.order_number = _pref.new_order_num()
            kwargs['force_insert'] = False

        super(Order, self).save(*args, **kwargs)


    def gen_invoice(self, request):
        """
        Write invoice header + details
        """
        if not self.is_active:
            log.info("SKIP inactive order: {}".format(self))
            return

        invoice = None
        next_invoice_date = self.get_next_invoice_date()
        if next_invoice_date > date.today():
            log.info("SKIP order not ready: {}, bill freq: {}, invoice due: {}"\
                .format(self, self.bill_frequency, next_invoice_date)
            )
            return

        for order_item in self.order_lines.all():
            # Create invoice once per order
            if not hasattr(invoice, "pk"):
                invoice = Invoice.objects.create(
                    order=self,
                    invoice_date=date.today(),
                    tenant = request.user,
                )
                log.info("Creating invoice: {}  #{}".format(self, invoice.number))

            _item = InvoiceItem.objects.create(
                invoice=invoice,
                charge_item=order_item.charge_item,
                quantity=order_item.quantity,
                tax_1=order_item.tax_1,
                tax_2=order_item.tax_2,
            )
        pref = Preference.objects.get(tenant=request.user)
        if invoice:
            self.last_invoice_date = invoice.invoice_date
            self.last_invoice = invoice
            self.save()
            log.info("Generating PDF, order: {}, invoice: {}".format(self, invoice))
            invoice.generate_pdf(pref)
        else:
            log.error("No invoice created: {}".format(invoice))

    def get_next_invoice_date(self):
        if not self.last_invoice_date and self.bill_frequency is None:
            return self.start_date

        if self.last_invoice_date and self.bill_frequency is None:
            return None

        _delta = self.BILL_FREQ[self.bill_frequency]
        if not self.last_invoice_date:
            return self.start_date + _delta
        else:
            return self.last_invoice_date + _delta

    @property
    def next_invoice_date(self):
        return self.get_next_invoice_date()


    @property
    def is_active(self):
        if not self.status == "1" or self.draft: return False
        if self.close_date: return False
        if self.last_invoice_date is None: return True
        if self.last_invoice_date and not self.bill_frequency:
            return False
        return True


class OrderItem(ModelBase):
    """
    Depending on the charge_item type, some items are recurring (re-invoicable)
    """
    order = models.ForeignKey("Order", related_name="order_lines")
    invoice_date = models.DateField(null=True, blank=True, editable=False)
    tax_1 = models.ForeignKey("Tax", null=True, blank=True, related_name="tax_1")
    tax_2 = models.ForeignKey("Tax", null=True, blank=True, related_name="tax_2")

    charge_item = models.ForeignKey("ChargeItem", null=False)
    quantity = models.DecimalField(
        _(u"quantity"), max_digits=8,
        decimal_places=2, default=1,
    )

    def __str__(self):
        return "{} x {}".format(self.charge_item, self.quantity)


    def clean(self):
        if not self.order:
            raise ValidationError('Need a valid order')

        if not hasattr(self.order, "recipient"):
            raise ValidationError('No recipient')

        if self.tax_1 and self.tax_1 == self.tax_2:
            raise ValidationError({
                'tax_2': 'Duplicate tax'
            })


    @property
    def unit_price(self):
        if self.charge_item:
            return "{:.2f}".format(self.charge_item.unit_price)
        else:
            return ""

    @property
    def line_cost(self):
        if self.quantity:
            return "{:.2f}".format(self.charge_item.unit_price * self.quantity)
        else:
            return ""