from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from django.contrib.auth.models import User

from .base import TenantBase


class Client(TenantBase):
    """
    N.B. Multiple address & social media
    """
    DAY = [(_, _ ) for _ in range(1, 32)]
    MONTH = [(_, _ ) for _ in range(1, 13)]
    user = models.ForeignKey(User, null=True, blank=True)  # Clients can be authenticated Django users
    company_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    notes = models.TextField(blank=True, null=True)
    last_invoice_date = models.DateField(null=True, blank=True, db_index=True)

    # Invoice details
    email = models.EmailField("email invoice", null=True, blank=True)
    address_line_1 = models.CharField(max_length=100, null=True, blank=True)
    address_line_2 = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=60)
    state_province = models.CharField(max_length=40, null=True, blank=True)
    postcode_zip = models.CharField(max_length=20)
    country = CountryField()
    website = models.URLField(blank=True, null=True)

    def __str__(self):
        return "{}".format(self.company_name)

    @property
    def invoice_email(self):
        if self.email:
            return self.email
        if self.user:
            return self.user.email

    def clean(self):
        if self.email or self.user.email:
            return

        raise ValidationError({
            'email': 'Specify invoice email here, or enter email on associated user'
        })

    @property
    def invoice_first_name(self):
        if self.first_name:
            return self.first_name.capitalize()

        if self.user and self.user.first_name:
            return self.user.first_name.capitalize()

        return "Sir/Madam"


    @property
    def invoice_full_name(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name.capitalize(), self.last_name.capitalize())

        if self.user and self.user.first_name and self.user.last_name:
            return "{} {}".format(self.user.first_name.capitalize(), self.user.last_name.capitalize())

        if self.first_name:
            return self.first_name.capitalize()

        if self.user and self.first_name:
            return self.first_name.capitalize()

        return "Sir/madam"
