from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible, smart_text

@python_2_unicode_compatible
class Currency(models.Model):
    code = models.CharField(unique=True, max_length=3)
    pre_symbol = models.CharField(blank=True, max_length=1)
    post_symbol = models.CharField(blank=True, max_length=1)

    def __str__(self):
        return self.code

    def format(self, amount):
        if self.pre_symbol:
            return smart_text("{}{:.2f}".format(self.pre_symbol, amount))
        if self.post_symbol:
            return smart_text("{:.2f}{}".format(amount, post_symbol))
        return smart_text("{:.2f} {}".format(amount, code))

    class Meta:
        verbose_name = _(u"currency")
        verbose_name_plural = _(u"currencies")


