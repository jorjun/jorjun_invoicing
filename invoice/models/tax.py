from django.db import models
from django.utils.translation import ugettext_lazy as _


class Tax(models.Model):
    tax_title = models.CharField(unique=True, max_length=30)
    tax_percent = models.DecimalField(
        _(u"rate %"), max_digits=4,
        decimal_places=2
    )

    def __str__(self):
        return "{} {} %".format(self.tax_title, self.tax_percent)

    class Meta:
        verbose_name_plural = _(u"tax")

