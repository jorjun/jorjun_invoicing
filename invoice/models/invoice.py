from __future__ import unicode_literals

from datetime import date
from decimal import Decimal
from dateutil.relativedelta import relativedelta

import hashlib

try:
    from email.mime.image import MIMEImage  # Py3
except ImportError:
    from email.MIMEImage import MIMEImage

from os.path import join, isfile

from django.db import models
from django.db.models import Max
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail import get_connection
from django.template.loader import get_template
from django.template import Context
from django.utils.translation import ugettext_lazy as _
from django.core import urlresolvers
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible, smart_text
from django.core.urlresolvers import reverse

import mandrill

from ..pdf import PDF
from .currency import Currency
from .prefs import Preference
from .paymethod import PayMethod
from .base import ModelBase, TenantBase
from ..utils import sign
from ..exception import *

import logging
log = logging.getLogger(__name__)

mandrill_client = mandrill.Mandrill(settings.EMAIL_HOST_PASSWORD)


@python_2_unicode_compatible
class Invoice(TenantBase):
    STATUS_CHOICE = (
        ("1", _("Active")),
        ("0", _("Inactive")),
        ("9", _("Archive")),
    )
    status = models.CharField(choices=STATUS_CHOICE, max_length=1, default="1", db_index=True)
    number = models.IntegerField(_(u'number'), db_index=True, blank=True, default=0, editable=False)
    invoice_id = models.CharField(_(u'invoice ID'), unique=False, max_length=10,
                                  null=True, blank=True, editable=False)
    invoice_date = models.DateField(_(u'invoice date'), default=date.today, editable=False, db_index=True)
    order = models.ForeignKey("Order", editable=False, related_name="original_order")
    payment_date = models.DateField(blank=True, null=True, editable=False) # Paid invoice
    sent_date = models.DateField(_("email on"), blank=True, null=True, editable=False) # Email'd invoice
    cancel_reason = models.TextField(_("To cancel, give reason"), blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.invoice_id)

    class Meta:
        ordering = ('-invoice_date', 'id')
        verbose_name = _(u"invoice")
        verbose_name_plural = _(u"invoices")
        unique_together = ('tenant', 'invoice_id', )

    def save(self, *args, **kwargs):

        # During the invoice creation we compute the ID with the pk (that's
        # why we save the model before)
        if not self.invoice_id:
            super(Invoice, self).save(*args, **kwargs)
            _pref = Preference.objects.get(tenant=self.tenant)
            self.number = self._get_next_number()
            self.invoice_id = _pref.new_invoice_num()
            kwargs['force_insert'] = False

        # We check if the invoice is paid
        if self.order.credit_invoice:
            # Consider it as "paid" since nobody need
            # to pay for it
            self.payment_method = None
            self.payment_date = date.today()
            self.order.credit_invoice.save()
        else:
            try:
                total_paid = sum([_.amount for _ in self.payments.all()])
            except InvoicePayment.DoesNotExist:
                total_paid = 0

            if (total_paid >= self.total()):
                self.payment_date = date.today()
            else:
                self.payment_date = None

        if self.cancel_reason:
            self.status = "0"
        else:
            self.status = "1"

        super(Invoice, self).save(*args, **kwargs)

    @property
    def signature(self):
        return sign(self.invoice_id, self.tenant_id)

    def check_signature(self, sig):
        if sig != self.signature:
            raise BadSignature()

    @property
    def total_paid(self):
        _total_paid = sum([_.amount for _ in self.payments.all()])
        return _total_paid

    @property
    def is_overdue_payment(self):
        if self.payment_date:
            return False

        _overdue = self.invoice_date + relativedelta(months=1)
        if date.today() > _overdue:
            return True
        return False

    @property
    def days_overdue(self):
        _overdue = (date.today() - self.invoice_date).days
        return _overdue


    def _get_next_number(self):
        """
        Return next invoice number - counting sequentially from 1, for current invoice year.

        .. warning::

            This is only used to prepopulate ``number`` field on saving new invoice.
            To get invoice number always use ``number`` field.

        .. note::

            To get invoice full number use ``invoice_id`` field.

        :return: string (generated next number)
        """

        # Recupere les facture de l annee
        relative_invoices = Invoice.objects.filter(tenant=self.tenant, invoice_date__year=self.invoice_date.year)
        # on prend le numero le plus eleve du champs number, sinon on met 0
        last_number = relative_invoices.aggregate(Max('number'))['number__max'] or 0

        return last_number + 1

    def total_amount(self):
        return self.total()
    total_amount.short_description = _(u"total amount")

    def last_payment(self):
        payments = self.payments.order_by('-paid_date')
        if payments:
            return payments[0]
        else:
            return None
    last_payment.short_description = _(u"last payment")

    def total(self):
        total = Decimal('0.00')
        for item in self.items.all():
            total = total + item.line_cost()
        return total
    total.short_description = _(u"total")

    def tax(self, tax):
        _total = sum(_.tax(tax) for _ in self.items.all())
        return _total


    def tax_total(self):
        _total = sum(_.tax_total() for _ in self.items.all())
        return _total

    def final_total(self):
        _total = sum(_.final_total() for _ in self.items.all())
        return _total
    final_total.short_description = _(u"invoice total")

    def balance_integer(self):
        """
        Whole units for Stripe payment method
        """
        return int(self.balance() * 100)

    def balance(self):
        return self.final_total() - self.total_paid

    @property
    def balance_formatted(self):
        return self.order.currency.format(self.balance())

    def file_name(self):
        return u'invoice_%s_%s.pdf' % (self.tenant, self.invoice_id)

    def generate_pdf(self, pref):
        with PDF(path=self.pdf_path(), invoice=self, pref=pref) as pdf:
            pdf.draw_pdf()

    def is_pdf_generated(self):
        return isfile(self.pdf_path())

    def pdf_path(self):
        _path = join(settings.INV_PDF_DIR, self.file_name())
        return _path

    def send_invoice(self, pref):
        to_email = self.order.recipient.email
        if not to_email:
            raise Exception("email missing, invoice: {}".format(self))

#        text_tmpl = get_template('invoice/email/invoice_email.txt')
        html_tmpl = get_template('invoice/email/invoice_email.html')
        subject = "Invoice [{}]".format(self.invoice_id)
        email_context = Context({
            'invoice': self,
            'date': date.today(),
            "SITE_NAME": settings.SITE_NAME,
            "item_list": [_ for _ in self.items.all()],
        })
        message = {
            'to': [
                {'email': to_email},
            ],
            'from_email': 'billing@jorjun.com',
            "subject": subject,
            "html" : html_tmpl.render(email_context),
            "auto_text": True,
            "headers": {'Reply-To': 'billing@jorjun.com'},
        }
        log.debug("[Mandrill] Send to: {}, subject: {}".format(to_email, subject))

        resp = mandrill_client.messages.send(
            message=message,
            async=False,
        )
        log.debug("[Mandrill] API response: %s" % resp)
        if resp[0].get("reject_reason"):
            raise Exception("Error: {}, to email: {}".format(resp[0]["reject_reason"], to_email))

        self.sent_date = date.today()
        self.save()

    def _update_is_paid(self):
        # Call the save() method in order to compute is the invoice is paid
        self.save()

    @property
    def payment_method_list(self):
        return PayMethod.get_payment_method_list(tenant=self.tenant)

    @property
    def logo(self):
        return Preference.objects.get(tenant=self.tenant).logo.name

    @property
    def href(self):
        return reverse(
            viewname="invoice_preview",
            kwargs={
                "tenant_id":self.tenant_id,
                "invoice_id":self.invoice_id,
                "sig":self.signature,
            }
        )


@python_2_unicode_compatible
class InvoiceItem(ModelBase):
    invoice = models.ForeignKey("Invoice", related_name='items', unique=False,
                                verbose_name=_(u'invoice'))
    charge_item = models.ForeignKey("ChargeItem")
    quantity = models.DecimalField(_(u"quantity"), max_digits=8,
                                   decimal_places=2, default=1)
    tax_1 = models.ForeignKey("Tax", null=True, blank=True, related_name="invoice_tax_1")
    tax_2 = models.ForeignKey("Tax", null=True, blank=True, related_name="invoice_tax_2")

    class Meta:
        verbose_name = _(u"invoice item")
        verbose_name_plural = _(u"invoice items")

    def __str__(self):
        return "{} x {}  {}".format(
            self.charge_item.code,
            self.quantity,
            self.invoice.order.currency.format(self.final_total()),
        )

    def line_cost(self):
        total = Decimal(str(self.charge_item.unit_price * self.quantity))
        return total.quantize(Decimal('0.01'))

    def tax(self, tax):
        assert tax in ("tax_1", "tax_2")
        tax = getattr(self, tax)
        if not tax:
            return 0

        if not tax.tax_percent:
            return ""
        perc =  tax.tax_percent / 100
        _total = Decimal(str(self.charge_item.unit_price * self.quantity * perc))
        return _total.quantize(Decimal('0.01'))

    def tax_total(self):
        _tax = [self.tax(tax=_) for _ in ("tax_1", "tax_2")]
        return sum(_tax)

    def final_total(self):
        _tax = sum([self.tax_total(), self.line_cost()])
        return _tax


class InvoicePayment(ModelBase):
    invoice = models.ForeignKey("Invoice", related_name='payments', unique=False,
                                verbose_name=_(u'invoice'))
    amount = models.DecimalField(_(u"amount"), max_digits=8,
                                 decimal_places=2)
    paid_date = models.DateField(_(u"paid date"), default=date.today())
    method = models.ForeignKey(PayMethod, null=True)
    additional_info = models.CharField(_(u"additional informations"),
                                       max_length=100, blank=True,
                                       null=True,
                                       help_text=_(u"eg. payment id"))
    is_exported = models.BooleanField(_(u"Is exported"), default=False,
                                      editable=False)

    def __str__(self):
        return self.method.get_method_display()

    class Meta:
        verbose_name = _(u"invoice payment")
        verbose_name_plural = _(u"invoice payments")


@receiver(post_save, sender=InvoicePayment)
def payment_saved(sender, instance, using, **kwargs):
    payment = instance
    payment.invoice._update_is_paid()


@receiver(post_delete, sender=InvoicePayment)
def payment_deleted(sender, instance, using, **kwargs):
    payment = instance
    payment.invoice._update_is_paid()


@receiver(post_save, sender=InvoiceItem)
def item_saved(sender, instance, using, **kwargs):
    item = instance
    item.invoice._update_is_paid()


@receiver(post_delete, sender=InvoiceItem)
def item_deleted(sender, instance, using, **kwargs):
    item = instance
    item.invoice._update_is_paid()

