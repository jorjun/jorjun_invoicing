from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from django.contrib.auth.models import User

from .base import TenantBase



class PayMethod(TenantBase):
    METHOD_CHOICES = (
        ('cheque', _(u'cheque')),
        ('transfer', _(u'bank transfer')),
        ('stripe', _(u'stripe')),
        ('bitcoin', _(u'bitcoin')),
        ('cash', _(u'cash')),
        ('dash', _(u'dash')),
    )
    STATUS_CHOICES = (
        ("1", "active"),
        ("0", "inactive"),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default="1", db_index=True)
    method = models.CharField(max_length=15, choices=METHOD_CHOICES, db_index=True)

    unique_together = ('tenant', 'method', )

    def __str__(self):
        return self.method

    @staticmethod
    def get_payment_method_list(tenant):
        meths = [_.method for _ in PayMethod.objects.filter(status="1", tenant=tenant)]
        assert meths, "No payment method found for tenant: %s" % tenant
        return meths