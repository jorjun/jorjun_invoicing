from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from django_extensions.db.fields import ModificationDateTimeField, CreationDateTimeField, AutoSlugField


class ModelBase(models.Model):
    created_time = CreationDateTimeField(_('created'))
    modified_time = ModificationDateTimeField(_('modified'))

    class Meta:
        abstract = True


class TenantBase(ModelBase):
    """
    Ensure that only tenant users can see/adjust records they create
    """
    tenant = models.ForeignKey(User, editable=False)

    class Meta:
        abstract = True

__all__ = ['ModelBase', 'TenantBase']