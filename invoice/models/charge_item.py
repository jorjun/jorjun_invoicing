from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import ModelBase, TenantBase


class ChargeItem(TenantBase):
    """
    N.B. Don't change price, change existing status to "0", create new product

    No stock control
    """
    STATUS_CHOICE = (
        ("1", _("Active")),
        ("0", _("Inactive")),
    )
    status = models.CharField(choices=STATUS_CHOICE, max_length=1, default="1", db_index=True, editable=False)
    code = models.CharField(_("code"), max_length=15, db_index=True)
    description = models.CharField(_(u"description"), max_length=255)
    unit_price = models.DecimalField(
        _(u"unit price"), max_digits=8,
        decimal_places=2
    )

    def __str__(self):
        return '{}'.format(self.code)
