from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError

from invoice.models.order import *


class OrderAdminForm(ModelForm):
    bill_times = forms.CharField(widget=forms.TextInput({ "placeholder": "infinite" }), required=False)

    class Meta(object):
        model = Order
        exclude = ("status", "order_id", "order_number")

    def clean_bill_times(self):
        if not self.cleaned_data["bill_times"]:
            return None
        else:
            return self.cleaned_data["bill_times"]

