from datetime import datetime
from dateutil import relativedelta

import django
from django.test import TestCase
from django.contrib.auth.models import User
from django.conf import settings

from invoice.models.invoice import Invoice, InvoiceItem
from invoice.models.charge_item import ChargeItem
from invoice.models.order import Order, OrderItem
from invoice.models.client import Client
from invoice.models.prefs import Preference
from invoice.pdf import PDF

django.setup()

class InvoiceTestCase(TestCase):
    """
    python jorjun/dj/manage.py dumpdata invoice --format=yaml --indent=4 >jorjun/dj/invoice/fixtures/test1.yml

    """

    fixtures = "test1"

    def setUp(self):
        self.mrt = ChargeItem.objects.first()
        self.item = OrderItem.objects.first()
        self.assertIsNone(self.item.invoice_date)
        self.user_a = User.objects.create(
            username="user_a",

        )
        self.pref_a = Preference.objects.get(tenant__username="jorjun")
        self.today = datetime.today()

    def testNewItemInvoice(self):
        """
        Generate invoice record from monthly order item
        """
        self.item.last_invoice_date = self.today

    def test_pdf_gen(self):
        inv = Invoice.objects.get(invoice_id=100103)
        with PDF(path="/tmp/test1.pdf", invoice=inv, pref=self.pref_a) as pdf:
            pdf.draw_pdf()


    #def test_email(self):
        #from django.core.mail import send_mail
        #send_mail('test email', 'hello world', 'jorjuntech@mailchimp.com', ['jorjuntech@icloud.com'])