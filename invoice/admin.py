# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta

from django.contrib import admin
from django.conf.urls import patterns, url
from django.db.models.loading import get_model
from django.utils.translation import ugettext_lazy as _

# Break PEP8 for Models & Forms
from invoice.models.invoice import *
from invoice.models.order import *
from invoice.models.currency import *
from invoice.models.client import *
from invoice.models.paymethod import *
from invoice.models.prefs import *
from invoice.models.tenant import *
from invoice.forms import *
from invoice.views import pdf_dl_view, pdf_gen_view

from invoice.admin_actions import send_invoice, generate_invoice



class RecipientFilter(admin.SimpleListFilter):
    title = _("recipient")
    parameter_name = 'recipient'

    def lookups(self, request, model_admin):
        return (
            (_.id, _.company_name) for _ in Client.objects.filter(tenant=request.user)
        )

    def queryset(self, request, queryset):
        recipient_id = self.value()
        qry = queryset.filter(tenant=request.user)
        if recipient_id:
            qry = qry.filter(recipient_id=int(recipient_id))
        return qry


class TenantRestrictBase(admin.ModelAdmin):
    """
    Abstract
    Always save `request.user` to `tenant` so we can Chinese Wall
    """
    def save_model(self, request, instance, form, change):
        instance = form.save(commit=False)
        if not change or not instance.tenant:
            instance.tenant = request.user

        instance.save()
        form.save_m2m()
        return instance


    def get_queryset(self, request):
        qs = super(TenantRestrictBase, self).get_queryset(request)
        if not request.user.is_superuser:
            return qs.filter(tenant=request.user)
        return qs


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'recipient':
            kwargs["queryset"] = Client.objects.filter(tenant=request.user)
        elif db_field.name == 'charge_item':
            kwargs["queryset"] = ChargeItem.objects.filter(tenant=request.user)

        return super(TenantRestrictBase, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


class ChargeItemAdmin(TenantRestrictBase):
    model = ChargeItem
    list_display = ('status', 'code', 'description', 'unit_price')
    fieldsets = [
        (None, {
            'fields': ['code', 'description', 'unit_price']
        }),
    ]

class InvoiceItemInline(admin.TabularInline):
    model = InvoiceItem
    extra = 0
    fieldsets = [
        ("Details", {
            'fields': [
                'charge_code',
                'charge_description',
                'quantity',
                'tax_1',
                'tax_2',
                'final_total',
            ]
        }),
    ]
    def has_delete_permission(self, request, obj):
        return False

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        return ['charge_code', 'charge_description', 'quantity', 'tax_1', 'tax_2', 'final_total']

    def charge_code(self, obj):
        return obj.charge_item.code

    def charge_description(self, obj):
        return obj.charge_item.description



class TaxAdmin(admin.ModelAdmin):
    model = Tax
    list_display = ('tax_title', 'tax_percent')


class ClientAdmin(TenantRestrictBase):
    model = Client
    list_display = ('tenant', 'user', 'invoice_full_name', 'invoice_email', 'company_name', )

    fieldsets = [
        ("Details", {
            'fields': [
                'user',
                'first_name',
                'last_name',
                'email',
                'company_name',
                'notes',
            ]
        }),
        ('Address', {
            'fields': [
                'address_line_1',
                'address_line_2',
                'city',
                'state_province',
                'postcode_zip',
                'country',
                'website',
            ]
        })
    ]


class CurrencyAdmin(admin.ModelAdmin):
    model = Currency
    list_display = ('code', 'pre_symbol', 'post_symbol')


class OrderItemInline(admin.TabularInline):
    extra = 1
    model = OrderItem
    inline_classes = ('collapse open',)

    fields = ["charge_item", "description", "quantity", "unit_price", "tax_1", "tax_2", "line_cost"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Limit foreign key choices to tenant.
        """
        if db_field.name == 'recipient':
            kwargs["queryset"] = Client.objects.filter(tenant=request.user)
        elif db_field.name == 'charge_item':
            kwargs["queryset"] = ChargeItem.objects.filter(tenant=request.user)

        return super(OrderItemInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def get_readonly_fields(self, request, obj=None):
        return ['unit_price', 'line_cost', 'description']

    def description(self, obj):
        return obj.charge_item.description


class InvoicePaymentInline(admin.StackedInline):
    model = InvoicePayment
    extra = 0


class OrderAdmin(TenantRestrictBase):
    inlines = [OrderItemInline]
    form =  OrderAdminForm
    list_filter = [RecipientFilter, 'status', 'start_date', 'draft', 'currency']

    list_display = (
        'status',
        'tenant',
        'order_number',
        'recipient',
        'start_date',
        'draft',
        'currency',
        'bill_frequency',
        'bill_times',
        'value',
        'last_invoice_date',
        'latest_invoice',
        'next_invoice',
    )

    actions = [generate_invoice, ]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['status', 'recipient', 'next_invoice_date', ]
        else:
            return ['status', ]

    def next_invoice(self, obj):
        if obj.is_active and obj.next_invoice_date <= date.today():
            return '<div style="background-color:green;color:white;">%s</div>' % obj.next_invoice_date
        elif not obj.is_active and obj.next_invoice_date is None:
            return '<div style="background-color:blue;color:white;">%s</div>' % _("Invoiced")
        else:
            return obj.next_invoice_date
    next_invoice.allow_tags = True

    def latest_invoice(self, obj):
        if obj.last_invoice:
            return "<a href='/admin/invoice/invoice/{}/'>{}</a>".format(obj.last_invoice.id, obj.last_invoice.invoice_id)
        else:
            "N/A"
    latest_invoice.allow_tags = True

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Limit foreign key choices to tenant.
        """
        if db_field.name == 'credit_invoice':
            kwargs["queryset"] = Invoice.objects.filter(tenant=request.user)

        return super(OrderAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

class InvoiceAdmin(TenantRestrictBase):
    model = Invoice
    inlines = [InvoiceItemInline, InvoicePaymentInline, ]

    search_fields = ('invoice_id',)
    list_filter = ['status', 'invoice_date', 'payment_date', ]
    list_display = (
        'status',
        'invoice_id',
        'order_id',
        'email',
        'recipient',
        'total_amount',
        'invoice_date',
        'sent',
        'balance',
        'paid_date',
    )
    actions = [send_invoice, ]

    def sent(self, obj):
        if obj.sent_date:
            return '<div style="background-color:blue;color:white;">%s</div>' % obj.sent_date
    sent.allow_tags = True

    def order_id(self, obj):
        return '<a href= "/admin/invoice/order/{}/">{}</a>'.format(obj.order_id, obj.order.order_number)
    order_id.allow_tags = True

    def get_readonly_fields(self, request, obj=None):
        return ['status', 'invoice_date', 'sent_date', 'final_total', 'balance', ]


    def get_urls(self):
        urls = super(InvoiceAdmin, self).get_urls()
        wrapped_pdf_dl_view = self.admin_site.admin_view(pdf_dl_view)
        wrapped_pdf_gen_view = self.admin_site.admin_view(pdf_gen_view)
        urls = patterns(
            '',
            (r'^(.+)/pdf/download/$', wrapped_pdf_dl_view),
            (r'^(.+)/pdf/generate/$', wrapped_pdf_gen_view),
        ) + urls
        return urls

    def recipient(self, obj):
        return obj.order.recipient

    def email(self, obj):
        return obj.order.recipient.invoice_email

    def paid_date(self, obj):
        if obj.payment_date:
            return '<div style="background-color:white;color:green;">%s</div>' % obj.payment_date
        if obj.is_overdue_payment:
            return '<div style="background-color:white;color:red;">%s</div>' % obj.days_overdue
        else:
            return '<div style="background-color:white;color:blue;">%s</div>' % obj.days_overdue

    paid_date.allow_tags = True
    paid_date.short_description = _(u"days overdue/paid")


class PreferenceAdmin(TenantRestrictBase):
    model = Preference
    list_display = ('tenant', 'company_name', 'company_phone', 'company_email', 'logo', 'last_invoice_num')

    fieldsets = [
        ("Invoice header", {
            'fields': [
                'logo',
                'company_name',
                'company_address_1',
                'company_address_2',
                'company_city',
                'company_state',
                'company_postcode',
                'company_country',
                'company_phone_prefix',
                'company_phone',
                'company_email_prefix',
                'company_email',
                'company_site_url',
                'company_reg_prefix',
                'company_reg_no',
                'company_vat_prefix',
                'company_vat_reg_no',
                'last_order_num',
                'last_invoice_num',
            ]
        }),
        ('Invoice footer', {
            'fields': [
                'footer_1',
                'footer_2',
            ]
        })
    ]


#class TenantAdmin(admin.ModelAdmin):
    #list_display = ['user', ]
    #def formfield_for_foreignkey(self, db_field, request, **kwargs):
        #"""
        #Limit new tenant selection to non-tenants
        #"""
        #if db_field.name == 'user':
            #tenant_id_list = [_.user.id for _ in Tenant.objects.all()]
            #kwargs["queryset"] = User.objects.exclude(id__in=tenant_id_list)

        #return super(TenantAdmin, self).formfield_for_foreignkey(
            #db_field, request, **kwargs
        #)


class PayMethodAdmin(TenantRestrictBase):
    model = PayMethod



admin.site.register(Tax, TaxAdmin)
admin.site.register(ChargeItem, ChargeItemAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Preference, PreferenceAdmin)
admin.site.register(PayMethod, PayMethodAdmin)
#admin.site.register(Tenant, TenantAdmin)