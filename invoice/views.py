from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse

from invoice.models.invoice import Invoice
from invoice.models.prefs import Preference
from .pdf import PDF
from invoice.utils import pdf_response

from os.path import getsize
from django.core.servers.basehttp import FileWrapper


def pdf_dl_view(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)

    if invoice.is_pdf_generated():
        # Serving file is not Django's job
        # If this need an optimisation, see X-sendfile mod (Apache/nginx)
        wrapper = FileWrapper(open(invoice.pdf_path(), 'rb'))
        response = HttpResponse(wrapper, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s"' %\
            invoice.file_name()
        response['Content-Length'] = getsize(invoice.pdf_path())
        return response
    else:
        messages.add_message(request, messages.ERROR,
                             _(u"You have to generate the PDF before!"))
    return HttpResponseRedirect("/admin/invoice/invoice/%s/" % invoice.id)


def pdf_gen_view(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)
    pref = Preference.objects.get(tenant=request.user)
    try:
        invoice.generate_pdf(pref=pref)
        messages.add_message(request, messages.INFO,
            _(u"PDF generated.")
        )
    except Exception as exc:
        messages.add_message(request, messages.ERROR,
            _(u"Error, PDF not generated: %s" % exc)
        )
    return HttpResponseRedirect("/admin/invoice/invoice/%s/" % invoice.id)


@login_required
def pdf_user_view(request, invoice_id):
    invoice = get_object_or_404(Invoice, invoice_id=invoice_id)
    return pdf_response(draw_pdf, invoice.file_name(), invoice)

